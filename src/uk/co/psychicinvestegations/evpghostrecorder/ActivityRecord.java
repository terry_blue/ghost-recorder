package uk.co.psychicinvestegations.evpghostrecorder;

import java.io.File;
import uk.co.psychicinvestegations.evpghostrecorder.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


public class ActivityRecord extends ActionBarActivity {

	final static String TAG = "ActivityRecord";
	
	
	DialogOpenFile openFileDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setIcon(new ColorDrawable(R.color.transparent));
		setContentView(R.layout.activity_record);		
		
		 // Look up the AdView as a resource and load a request.
	    AdView adView = (AdView) this.findViewById(R.id.adView);
	    AdRequest adRequest = new AdRequest.Builder().build();
	    adView.loadAd(adRequest);
		
		openFileDialog = new DialogOpenFile();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_record, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.menu_open_file) {
			FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
			Fragment prev = getSupportFragmentManager().findFragmentByTag(DialogOpenFile.TAG);
		    if (prev != null) {
		        ft.remove(prev);
		    }
		    ft.addToBackStack(null);		    
		    
		    new DialogOpenFile().show(ft, DialogOpenFile.TAG);
		    
			return true;
		}
		else if(id == R.id.menu_rename_file) {
			if(getTitle() != getResources().getString(R.string.app_name)) {
				//Show dialog with edit text
				final EditText input = new EditText(this);
				input.setText(getTitle());
				
				new AlertDialog.Builder(ActivityRecord.this)
			    .setTitle(R.string.rename)
			    .setMessage(R.string.rename_file)
			    .setView(input)
			    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int whichButton) {
			            Editable value = input.getText(); 
			            
			            File directory = new File(DialogOpenFile.getFilepath(ActivityRecord.this));
			            File from = new File(directory, (String)getTitle());
			            File to = new File(directory, value.toString());
			            from.renameTo(to);
			            
			            setTitle(value.toString());
			            
			            ((FragmentPlayer)getSupportFragmentManager().findFragmentById(R.id.fragment_record_player)).refreshFilenameList();
			        }
			    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int whichButton) {
			            // Do nothing.
			        }
			    }).show();
			}
		}
		else if(id == R.id.menu_delete_file && getTitle() != getResources().getString(R.string.app_name)) {			
			new AlertDialog.Builder(ActivityRecord.this)
				.setTitle(R.string.delete)
				.setMessage(getResources().getString(R.string.delete_file) + " " + getTitle() + "?")
				.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						new File(DialogOpenFile.getFilepath(ActivityRecord.this), (String)getTitle()).delete();
						((FragmentPlayer)getSupportFragmentManager().findFragmentById(R.id.fragment_record_player)).refreshFilenameList();
							
					}
				}).setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
						
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
							
					}
				}).show();				
			} else if(id == R.id.menu_share_file && getTitle() != getResources().getString(R.string.app_name)) {				
				String sharePath = DialogOpenFile.getFilepath(ActivityRecord.this)
									+ ((FragmentPlayer)getSupportFragmentManager()
											.findFragmentById(R.id.fragment_record_player))
											.getOpenFilename();
					
				Uri uri = Uri.parse("file://"+ sharePath);
				Intent share = new Intent(Intent.ACTION_SEND);
				share.setType("audio/*");
				share.putExtra(Intent.EXTRA_STREAM, uri);
				startActivity(Intent.createChooser(share, "Share Sound File"));
				
			} else if(id == R.id.menu_action_forum) {
				startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse("http://adf.ly/rOqjg")));
			}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void openFileInPlayer(String filename) {
		FragmentPlayer playerFragment = (FragmentPlayer)getSupportFragmentManager().findFragmentById(R.id.fragment_record_player);
		playerFragment.loadFile(filename);
		
	}
	
	
}
