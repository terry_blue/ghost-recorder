package uk.co.psychicinvestegations.evpghostrecorder;

import uk.co.blueshroom.sureants.PackageChecker;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Window;
import android.view.WindowManager;

public class ActivitySplash extends ActionBarActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
	    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_splash);	
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		new ShowSplashTask().execute();
	}
	
	private class ShowSplashTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			try { Thread.sleep(2000); }
			catch(Exception e) { }
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			new PackageChecker(getApplicationContext().getPackageName(), ActivitySplash.this, ActivityRecord.class).checkPackage();
		}
	}
}
