package uk.co.psychicinvestegations.evpghostrecorder;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class RadialEQ extends View {
	
	final static String TAG = "RadialEQ";

	float cx, cy;	
	
	final int MAX_RAD = 128;
	int[] radii;
	final public static byte SAMPLE_SIZE = 8;
	final int[] SAMPLE_COLOURS = new int[] { 0x22222222, 0x44444444, 0x66666666, 0x88888888, 0x99999999, 0xaaaaaaaa, 0xcccccccc, 0xeeeeeee };
	Paint[] cPaints;
	
	public RadialEQ(Context context, AttributeSet attrs) {
		super(context, attrs);
		cx = 0; cy = 0;		
		
		radii = new int[SAMPLE_SIZE];
		cPaints = new Paint[SAMPLE_SIZE];
		
		for(int i = 0; i < SAMPLE_SIZE; i++) {
			cPaints[i] = new Paint();
			cPaints[i].setColor(SAMPLE_COLOURS[i]);
			cPaints[i].setStrokeWidth(1);
			cPaints[i].setStyle(Paint.Style.STROKE);
		}		
	}

	
	
	@Override
	public void onDraw(Canvas canvas) {
		if(cPaints != null) {			
			for(int i = 0; i < SAMPLE_SIZE; i++) {
				canvas.drawCircle(cx, cy, radii[i], cPaints[i]);
			}
			
		}
	}
	
	
	
	public void updateRadii(byte[] waveForm) {
		cx = getWidth() /2f;
		cy = getHeight() / 2f;
		float max_dim = (getHeight() < getWidth()) ? (float)getHeight() / 2f: (float)getWidth() / 2f;
		
		for(int i = 0; i < SAMPLE_SIZE; i++) {
			radii[i] = (int)((((float)(Math.abs(waveForm[i])) / (float)MAX_RAD)) * max_dim);
			Log.e(TAG, "radii["+i+"] = " + radii[i]);
		}
		
		
		invalidate();
	}
	
	
	
}