package uk.co.psychicinvestegations.ghostrecorder;

import uk.co.blueshroom.sureants.PackageChecker;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class ActivitySplash extends ActionBarActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);	
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		new PackageChecker(getApplicationContext().getPackageName(), this, ActivityRecord.class).checkPackage();
	}
}
