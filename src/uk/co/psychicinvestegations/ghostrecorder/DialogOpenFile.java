package uk.co.psychicinvestegations.ghostrecorder;

import java.io.File;
import java.text.NumberFormat;

import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class DialogOpenFile extends DialogFragment implements OnItemClickListener{

	public final static String TAG = "DialogFragment"; 
	static DialogOpenFile dialogOpenFile = null;
	String[] theNamesOfFiles;

	public static DialogOpenFile getInstance() {
		if(dialogOpenFile != null)
			return dialogOpenFile;
		
		dialogOpenFile = new DialogOpenFile();
		return dialogOpenFile;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.dialog_open_file, container, false);
		
		return view;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public void onStart() {
		getDialog().setTitle(R.string.open_file);		
		
		super.onStart();
		
	}
	
	@Override
	public void onResume() {
		super.onResume();
		theNamesOfFiles = getFileNameList((ActionBarActivity)getActivity());
		
		ListView fileLV = ((ListView)getView().findViewById(R.id.openfile_listview_filelist));		
		
		fileLV.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, theNamesOfFiles));
		fileLV.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,	long id) {
		//Open selected file in the player
		((ActivityRecord)getActivity()).openFileInPlayer(theNamesOfFiles[position]);
		dismiss();
	}
	
	public static String[] getFileNameList(ActionBarActivity activ) {
		File dir = new File(getFilepath(activ));
		dir.mkdirs();
		File[] filelist = dir.listFiles();		
		
		String[] filenames = new String[filelist.length];
		for (int i = 0; i < filenames.length; i++) 
			filenames[i] = filelist[i].getName();		
		
		return filenames;
	}
	
	public static String getNewFilenameAndPath(ActionBarActivity activ) {
		final String SP_KEY_FILENAME_COUNT = "sharedpreferences_key_filename_count";
		
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activ);
		NumberFormat mNumberFormat = NumberFormat.getInstance();
		mNumberFormat.setMinimumIntegerDigits(3);
		
		String fileNumber = mNumberFormat.format(sp.getInt(SP_KEY_FILENAME_COUNT, 0));	
		
		SharedPreferences.Editor edit = sp.edit();
		edit.putInt(SP_KEY_FILENAME_COUNT, sp.getInt(SP_KEY_FILENAME_COUNT, 0) + 1);
		edit.commit();
		
		return getFilepath(activ) + activ.getResources().getString(R.string.filename_prefix)
				+ fileNumber;
	}
	
	public static String getFilepath(ContextWrapper cWrapper) {	
		Log.e(TAG, Environment.getExternalStorageDirectory().getAbsolutePath() + "/GhostRecords");
		return Environment.getExternalStorageDirectory().getAbsolutePath() + "/GhostRecords/";
	}
}
