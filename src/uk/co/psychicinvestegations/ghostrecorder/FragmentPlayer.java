package uk.co.psychicinvestegations.ghostrecorder;

import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;

import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentPlayer extends Fragment implements OnClickListener, OnSeekBarChangeListener {

	public static enum PlayerState { READY, PLAYING, PAUSED, RECORDING };
	PlayerState cPlayerState;
	
	private final String TAG = "FragmentPlayer";
	MediaPlayer mPlayer = null; 
	MediaRecorder mRecorder = null;
	
	String recFilepath;
	String[] filenameList;
	int filenameIndex;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_player,
				container, false);		
		
		rootView.findViewById(R.id.player_button_prev).setOnClickListener(this);
		rootView.findViewById(R.id.player_button_play).setOnClickListener(this);
		rootView.findViewById(R.id.player_button_next).setOnClickListener(this);
		rootView.findViewById(R.id.player_button_rec).setOnClickListener(this);
		
		((SeekBar)rootView.findViewById(R.id.player_seekbar)).setOnSeekBarChangeListener(this);
		
		refreshFilenameList();
		filenameIndex = 0;
		
		return rootView;
	}
	
	@Override
	public void onPause() {
		super.onPause();
		releaseMediaPlayer();
		releaseMediaRecorder();
		
	}
	@Override
	public void onResume() {
		super.onResume();
		
		changePlayerState(PlayerState.READY);
		
		if(filenameList.length > 0)
			loadFile(filenameList[filenameList.length - 1]);
	}
	
	
	public boolean loadFile(String filename) {
		releaseMediaPlayer();		
		
		for(int i = 0; i < filenameList.length; i++) {
			if(filename.equals(filenameList[i])) {
				filenameIndex = Integer.valueOf(i);
				break;
			}
		}
		
		mPlayer = new MediaPlayer();
		
		try {
			FileInputStream inputStream = new FileInputStream(DialogOpenFile.getFilepath(getActivity()) + "/" + filename);			
            mPlayer.setDataSource(inputStream.getFD());          
            inputStream.close();
            
            mPlayer.prepare();
            getActivity().setTitle(new File(filename).getName());   
            ((SeekBar)getView().findViewById(R.id.player_seekbar)).setMax(mPlayer.getDuration());
            ((TextView)getView().findViewById(R.id.player_textview_duration_max)).setText(getReadableTimeStamp(mPlayer.getDuration()));
            
            changePlayerState(PlayerState.READY);
            return true;
            
        } catch (Exception e) {
            Log.e(TAG, "loadFile() failed: " + e.toString());
        }
		
		return false;
	}
	
	public void play() {
        try {            
            mPlayer.start();           
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            
            new PlayUpdateTask().execute();
            changePlayerState(PlayerState.PLAYING);
        } catch (Exception e) {
            Log.e(TAG, "prepare() failed: " + e.toString());
        }
	}
	public void stopPlaying() {
		mPlayer.stop();
		releaseMediaPlayer();
		loadFile(filenameList[filenameIndex]);
		getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		changePlayerState(PlayerState.READY);
	}
	public void pausePlay() {        
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
		mPlayer.pause();		
		changePlayerState(PlayerState.PAUSED);
	}
	public void resumePlay() {
		getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		mPlayer.start();		
		changePlayerState(PlayerState.PLAYING);
	}
	
	public void startRecording() {		
		getView().findViewById(R.id.player_linearlayout_rec_indicator).setVisibility(View.VISIBLE);
		
		((ImageView)getView().findViewById(R.id.player_imageview_rec_indicator))
			.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rec_blink));		
		
		recFilepath = DialogOpenFile.getNewFilenameAndPath((ActionBarActivity)getActivity());
		releaseMediaRecorder();
		mRecorder = new MediaRecorder();
		mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(recFilepath);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		
        try {
            mRecorder.prepare();
            mRecorder.start();            
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);            
            changePlayerState(PlayerState.RECORDING);
            new RecordUpdateTask().execute();
            Toast.makeText(getActivity(), R.string.new_record_start, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "mRecorder prepare() failed: " + e.toString());
        }
	}
	
	public void stopRecording() {		
		mRecorder.stop();
		releaseMediaRecorder();
       
		getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);		
		
        String recFilename = recFilepath.substring(recFilepath.lastIndexOf('/') + 1);        
        Toast.makeText(getActivity(), recFilename + " saved", Toast.LENGTH_SHORT).show();
        loadFile(recFilename);
        
        ((ImageView)getView().findViewById(R.id.player_imageview_rec_indicator)).clearAnimation();
        getView().findViewById(R.id.player_linearlayout_rec_indicator).setVisibility(View.INVISIBLE);
        
        refreshFilenameList();
        changePlayerState(PlayerState.READY);
	}


	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.player_button_prev:
			if(filenameIndex > 0)
				filenameIndex -= 2;
			else filenameIndex = filenameList.length - 2;
		case R.id.player_button_next:
			if(filenameIndex < filenameList.length - 1)
				filenameIndex++;
			else filenameIndex = 0;
			
			boolean toPlay = (cPlayerState == PlayerState.PLAYING);			
			stopPlaying();
			try {
				loadFile(filenameList[filenameIndex]);
			} catch(Exception e) {  }
			if(toPlay) 
				play();
			
			
			break;
			
		case R.id.player_button_play:
			switch(cPlayerState) {
			case READY:
				play();
				break;
			case PLAYING:
				pausePlay();
				break;
			case PAUSED:
				resumePlay();
				break;
			}
			
			break;
		case R.id.player_button_rec:
			switch(cPlayerState) {
			case READY:
				startRecording();
				break;
			case PAUSED:
			case PLAYING:
				stopPlaying();
				break;
			case RECORDING:
				stopRecording();
				break;
			}
			break;
		}	
		
	}
	
	private void releaseMediaPlayer() {
		if(mPlayer != null) {
			mPlayer.release();
			mPlayer = null;
		}
	}
	private void releaseMediaRecorder() {
		if(mRecorder != null) {
			mRecorder.reset();
	        mRecorder.release();
	        mRecorder = null;
		}
	}
	public void refreshFilenameList() {
		filenameList = DialogOpenFile.getFileNameList((ActionBarActivity)getActivity());
		if(filenameList.length > 1) 
			loadFile(filenameList[(filenameIndex < filenameList.length - 1) ? filenameIndex : filenameList.length - 2]);
		else if(filenameList.length == 1)
			loadFile(filenameList[0]);
		
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if(fromUser && mPlayer != null)
			mPlayer.seekTo(progress);
		
		((TextView)getView().findViewById(R.id.player_textview_duration_elapsed)).setText(getReadableTimeStamp(progress));
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		
	}
	
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		
		
	}
	
	
	
	
	
	public void changePlayerState(PlayerState newPlayerState) {
		cPlayerState = newPlayerState;
		ImageButton playBtn = (ImageButton)getView().findViewById(R.id.player_button_play);
		ImageButton recBtn = (ImageButton)getView().findViewById(R.id.player_button_rec);
		
		switch(cPlayerState) {
		case READY:
			playBtn.setEnabled(true);
			playBtn.setImageResource(R.drawable.btn_play);
			recBtn.setImageResource(R.drawable.btn_rec);
			break;
		case PLAYING:
			playBtn.setEnabled(true);
			playBtn.setImageResource(R.drawable.btn_pause);
			recBtn.setImageResource(R.drawable.btn_stop);
			break;
		case RECORDING:
			playBtn.setEnabled(false);
			recBtn.setImageResource(R.drawable.btn_stop);
			break;
		case PAUSED:
			playBtn.setEnabled(true);
			playBtn.setImageResource(R.drawable.btn_play);
			break;
		}
	}
	
	
	public static String getReadableTimeStamp(long millisecs) {
		long mins = millisecs / 60000;
		long secs = (millisecs - (mins * 60000)) / 1000;
		DecimalFormat formatter = new DecimalFormat("#00.###");
		return Long.toString(mins) + " : " + formatter.format(secs);
	}
	
	
	public String getOpenFilename() {
		return filenameList[filenameIndex];
	}
	
	
	
	
	
	private class PlayUpdateTask extends AsyncTask<Void, Long, Void> {

		@Override
		protected Void doInBackground(Void... params) {			
			int total = mPlayer.getDuration();
			while(mPlayer != null && mPlayer.getCurrentPosition() < total) {
				try {
					Thread.sleep(100);
					publishProgress(Long.valueOf((mPlayer.getCurrentPosition())));
				} catch(Exception e) { Log.e(TAG, "PlayUpdateTask loop exception: " + e.toString()); }				
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Long... params) {
			((SeekBar)getView().findViewById(R.id.player_seekbar)).setProgress(params[0].intValue());
		}
	}
	
	
	private class RecordUpdateTask extends AsyncTask<Void, Long, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			final Long STIME = System.currentTimeMillis();
			while(mRecorder != null) {
				try { Thread.sleep(100); }
				catch(Exception e) { }
				publishProgress(System.currentTimeMillis() - STIME);
			}
			return null;
		}
		
		@Override
		protected void onProgressUpdate(Long... params) {
			((TextView)getView().findViewById(R.id.player_textview_rec_duration))
				.setText(getReadableTimeStamp(params[0]));
		}
		
		@Override
		protected void onPostExecute(Void param) {
			refreshFilenameList();
			loadFile(filenameList[filenameList.length - 1]);
		}
	}
}
